import asyncio
from typing import Optional
import BF2MasterServer
import signal
import logging
import sys
import ipaddress
import argparse



# Kill process on sigint
signal.signal(signal.SIGINT, signal.SIG_DFL)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Bind to this interface
    parser.add_argument("--bindto", default="127.0.0.1", type=str, help="Bind to this interface")
    # Only accept packets from this subnet
    parser.add_argument("--packetfilter", default="", type=str, help="Only accept packets from this subnet")
    args = parser.parse_args()



    bindTo = args.bindto

    if args.packetfilter == "":
        packetFilter = bindTo + "/32"
    else:
        packetFilter = args.packetfilter




    fileHandler = logging.FileHandler("proxylog.log")
    fileHandler.level = logging.WARNING
    fileHandler.setFormatter(logging.Formatter("[%(asctime)s] - %(levelname)s - %(message)s"))

    stdoutHandler = logging.StreamHandler(sys.stdout)
    stdoutHandler.level = logging.INFO
    stdoutHandler.setFormatter(logging.Formatter("[%(asctime)s] - %(levelname)s - %(message)s"))

    logging.basicConfig(level=logging.INFO,
                        datefmt="[%Y-%m-%d %H:%M:%S]",
                        handlers=[fileHandler, stdoutHandler])

    # Crash early if bad options
    if ipaddress.ip_address(bindTo) not in ipaddress.ip_network(packetFilter):
        raise Exception("Bad bind/filter configuration")

    loop = asyncio.get_event_loop()
    masterServer = BF2MasterServer.BF2MasterServer(bindTo, ipaddress.ip_network(packetFilter))
    loop.create_task(masterServer.start())
    loop.run_forever()
