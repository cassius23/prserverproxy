import asyncio
import ipaddress
import logging

class ServerListProxy(asyncio.DatagramProtocol):
    def __init__(self, serversManager, localIP, packetFilter):
        self.serversManager = serversManager
        self.localIP = localIP
        self.packetFilter = packetFilter

    def connection_made(self, transport):
        self.UDPTransport = transport
        logging.info("ServerList Proxy bound")

    def datagram_received(self, data, addr):
        if ipaddress.ip_address(addr[0]) not in self.packetFilter:
            return

        # Sent when server is launched. This is sent from an unallocated random port and never used again.
        # two attempts are made by the game client. It doesn't seem to really care about it
        if data == b'\t\x00\x00\x00\x00battlefield2\x00':
            self.UDPTransport.sendto(b"\xfe\xfd\x09\x00\x00\x00\x00", addr)


        # These come from the correct sv.GameSpyPort
        elif len(data) > 5:
            typ = data[0]
            #key = struct.unpack_from("<I", data, offset=1)[0]
            server = self.serversManager.getOrNew(addr[1])
            #server.setKey(key)

            # Ping
            if typ == 0x08:
                server.keepAlive()

            # Server Info packet
            elif typ == 0x03:
                server.parseInfo(str(data[5:], encoding="utf-8"))
                #print("Received info from %s" % server)

            # Challenge response - unused, everyone is valid for us.
            # elif typ == 0x01:
            #     self.UDPTransport.sendto(b"\xfe\xfd\x0a" + struct.pack("<I", key), addr)
                #print("Received challenge from %s" % server)